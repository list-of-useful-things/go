# Code

### Applications
* [Tania](https://github.com/Tanibox/tania-core) - Farm Management Software

### Tools
* [noverify](https://github.com/VKCOM/noverify) - Pretty fast linter (code static analysis utility) for PHP
* [latestgo](https://github.com/tmthrgd/latestgo) - A tool to automatically install the latest supported golang versions
* [GoReleaser](https://github.com/goreleaser/goreleaser) - Deliver Go binaries as fast and easily as possible
* [gRPC debugging tools](https://github.com/bradleyjkemp/grpc-tools) - A suite of gRPC debugging tools. Like Fiddler/Charles but for gRPC.

### Libraries
* [ebiten](https://github.com/hajimehoshi/ebiten) - A dead simple 2D game library in Go
* [httpheader](https://github.com/vfaronov/httpheader) - a Go package to parse and generate standard HTTP headers correctly

### Frameworks
* [Beego](https://beego.me/) - An open source framework to build and develop your applications in the Go way
* [Geziyor](https://github.com/geziyor/geziyor) - A fast web crawling & scraping framework for Go
* [httransform](https://github.com/9seconds/httransform) - A framework to build precise and fast HTTP proxies
* [coco](https://github.com/mrmiguu/coco) - Golang WebAssembly Framework

### PDF generation
* [Golang HTML to PDF Converter](https://github.com/Mindinventory/Golang-HTML-TO-PDF-Converter)
* [maroto](https://github.com/johnfercher/maroto) - A maroto way to create PDFs. Maroto is inspired in Bootstrap and uses gofpdf. Fast and simple
 
### Var
* [Field Services Golang Server](https://github.com/longfellowone/field-services)
* [Benthos](https://github.com/Jeffail/benthos) - A stream processor for dull stuff written in Go
* [lorca](github.com/zserge/lorca) - HTML5 desktop apps in GoLang
* [Pilosa](https://github.com/pilosa/pilosa) - an open source, distributed bitmap index that dramatically accelerates queries across multiple, massive data sets.
* [heimdall](https://github.com/gojek/heimdall) - An enhanced HTTP client for Go
* [Olivia](https://github.com/olivia-ai/olivia) - chatbot built with an artificial neural network
* [Nii Simple Server](https://github.com/nii236/simpleserver) - Simpleserver is a simple file server, that serves the folder you run it from. Generates a self signed certificate in memory for https.
* [go web app](https://github.com/talentlessguy/go-web-app) - CLI for setting up Go WebAssembly frontend app / [tutorial](https://dev.to/talentlessguy/create-frontend-go-apps-using-go-web-app-56cb)
* [qamel](https://github.com/RadhiFadlillah/qamel) - Simple QML binding for G
* [yaegi](https://github.com/containous/yaegi) - Yaegi is Another Elegant Go Interpreter

### Examples
* [Golang Microservices Example](https://github.com/harlow/go-micro-services)
* [Simple GoLang RestAPI](https://github.com/NukeDev/GoLang-RestAPI)
 
# Articles
* [A Word from The Beegoist - An In-depth Tutorial To Help Jumpstart Beego For You](https://medium.com/@richardeng/a-word-from-the-beegoist-d562ff8589d7)
* [A Word from The Beegoist II - The Sequel](https://medium.com/@richardeng/a-word-from-the-beegoist-ii-9561351698eb)
* [A Word from The Beegoist III - Every web application needs a database administrator](https://medium.com/@richardeng/a-word-from-the-beegoist-iii-dbd6308b2594)
* [Awesome Go Storage - list of awesome Go storage projects and libraries](https://github.com/gostor/awesome-go-storage)
* [Awesome Go - A curated list of awesome Go frameworks, libraries and software](https://github.com/avelino/awesome-go)
* [Microservices in Golang - Part 1](https://ewanvalentine.io/microservices-in-golang-part-1/)
* [Tutorial, Part 1 - How to develop Go gRPC microservice with HTTP/REST endpoint, middleware, Kubernetes deployment, etc.](https://medium.com/@amsokol.com/tutorial-how-to-develop-go-grpc-microservice-with-http-rest-endpoint-middleware-kubernetes-daebb36a97e9)
* [Microservices in Go](https://medium.com/seek-blog/microservices-in-go-2fc1570f6800)
* [Go: How Does defer Statement Work?](https://medium.com/@blanchon.vincent/go-how-does-defer-statement-work-1a9492689b6e)
* [A bird's eye view of Go](https://blog.merovius.de/2019/06/12/birdseye-go.html) - a very high-level overview of Go
* [The Beauty of Go: scalable services](https://telescope.ac/battlefield/the-beauty-of-go-1)
* [Reduce your WebAssembly binaries 72% - from 56KB to 26KB to 16KB ](https://dev.to/sendilkumarn/reduce-your-webassembly-binaries-72-from-56kb-to-26kb-to-16kb-40mi)
* [(Tiny)Go to WebAssembly](https://dev.to/sendilkumarn/tiny-go-to-webassembly-5168)
* [Intro to Modules on Go - Part 1](https://dev.to/prassee/intro-to-modules-on-go-part-1-1k77)
* [Simple techniques to optimise Go programs](https://stephen.sh/posts/quick-go-performance-improvements)
* [Netlify Form Submissions With Golang Lambda Functions](https://camilopayan.com/posts/netlify-form-submissions-with-golang-lambda-functions/)
* [The Top 10 Most Common Mistakes I’ve Seen in Go Projects](https://medium.com/@teivah/the-top-10-most-common-mistakes-ive-seen-in-go-projects-4b79d4f6cd65)
* [Practical guide to securing gRPC connections with Go and TLS — Part 1](https://itnext.io/practical-guide-to-securing-grpc-connections-with-go-and-tls-part-1-f63058e9d6d1)

# Design patterns
* [Decorator](https://www.reddit.com/r/golang/comments/cg4vj1/go_design_patterns_the_decorator_pattern_part_one/)

# Tutorials
* [Building a Chat Application in Go with ReactJS](https://tutorialedge.net/projects/chat-system-in-go-and-react/)
* [Let's build a SQL parser in Go!](https://marianogappa.github.io/software/2019/06/05/lets-build-a-sql-parser-in-go/) 
* [A simple guide to building REST API’s in GO](https://ashish.ch/a-simple-guide-to-creating-rest-apis-in-go/)
 
# Books
* [go-perfbook](https://github.com/dgryski/go-perfbook) - best practices for writing high-performance Go code

# Topics

### Cryptography
* [Introducing CIRCL: An Advanced Cryptographic Library](https://blog.cloudflare.com/introducing-circl/)

### Machine learning
* [Deep learning in Go](https://medium.com/@hackintoshrao/deep-learning-in-go-f13e586f7d8a)
* [cortex](https://github.com/cortexlabs/cortex) - Machine learning infrastructure for developers

### REST
* [VIDEO / Golang REST API With Mux](https://www.youtube.com/watch?v=SonwZ6MF5BE)

### Databases
* [couchdb-go](https://github.com/rhinoman/couchdb-go)